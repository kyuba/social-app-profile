# Social media profile  

Dummy social media profile written with the use of TypeScript, React 16.6 and Redux  
Uses ducks architecture for Redux  
Unit tests in Jest & Enzyme  

Bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Try it out
1. yarn install
2. yarn start
