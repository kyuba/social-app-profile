import { combineReducers } from "redux";

import commentsReducer, { IComment } from "./comments";
import layoutReducer, { ILayout } from "./layout";
import userReducer, { IUser } from "./user";

export interface IApplicationState {
  user: IUser;
  comments: IComment[];
  layout: ILayout;
}

export default combineReducers<IApplicationState>({
  user: userReducer,
  comments: commentsReducer,
  layout: layoutReducer,
});
