import { Action } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { LAYOUT_SHOW_ERROR } from "./layout";
import { IApplicationState } from "./rootReducer";

// Typings
export interface IComment {
  uid: string;
  username: string;
  date: string;
  message: string;
}

export interface ICommentsSetAction { type: typeof COMMENTS_SET; payload: IComment[]; }
export interface ICommentsAddACtion { type: typeof COMMENTS_ADD; payload: IComment; }

type CommentsReducerActions = ICommentsSetAction | ICommentsAddACtion;

// Actions
export const COMMENTS_SET   = "social-app-profile/comments/SET";
export const COMMENTS_ADD = "social-app-profile/comments/ADD";

// Action creators
export const setComments = () => async (dispatch: ThunkDispatch<IApplicationState, void, Action>): Promise<Action> => {
  try {
    const res = await fetch("__mocks__/comments.json");
    const json = await res.json();

    return dispatch({
      type: COMMENTS_SET,
      payload: json,
    });
  } catch (error) {
    return dispatch({
      type: LAYOUT_SHOW_ERROR,
      payload: error.message,
    });
  }
};

export const addComment = (comment: IComment) => ({
  type: COMMENTS_ADD,
  payload: comment,
});

// Reducer
const initialState = new Array();

export default function commentsReducer(state: IComment[] = initialState, action: CommentsReducerActions) {
  switch (action.type) {
    case COMMENTS_SET:
      return [...action.payload];
    case COMMENTS_ADD:
      return [...state, action.payload];
    default:
      return state;
  }
}
