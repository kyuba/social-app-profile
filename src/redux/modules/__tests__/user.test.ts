import userReducer, {
  IUser,
  IUserIncStatAction,
  IUserSetAction,
  USER_INC_SOCIAL_STAT,
  USER_SET,
} from "../user";

const initialState: IUser = {
  name: "",
  city: "",
  country: "",
  social: {
    likes: 0,
    following: 0,
    followers: 0,
  },
};

const mockUser = {
  name: "Harvey Specter",
  city: "Warsaw",
  country: "Poland",
  social: {
    likes: 140,
    following: 555,
    followers: 333,
  },
};

describe("userReducer", () => {
  it("sets user data", () => {
    const action: IUserSetAction = {
      type: USER_SET,
      payload: {...mockUser},
    };

    const expectedState = action.payload;

    expect(userReducer(initialState, action)).toEqual(expectedState);
  });

  it("increments selected social stat", () => {
    const action: IUserIncStatAction = {
      type: USER_INC_SOCIAL_STAT,
      payload: "likes",
    };

    const expectedState = {
      ...mockUser,
      social: {
        ...mockUser.social,
        likes: 141,
      },
    };

    expect(userReducer(mockUser, action)).toEqual(expectedState);
  });
});
