import layoutReducer, {
  ILayout,
  ILayoutHideModal,
  ILayoutShowModal,
  LAYOUT_HIDE_MODAL,
  LAYOUT_SHOW_MODAL,
} from "../layout";

const initialState: ILayout = {
  hasError: false,
  isModalOpen: false,
  errorMsg: "",
};

describe("layoutReducer", () => {
  it("shows modal", () => {
    const action: ILayoutShowModal = {
      type: LAYOUT_SHOW_MODAL,
    };

    const expectedState = {...initialState, isModalOpen: true};

    expect(layoutReducer(initialState, action)).toEqual(expectedState);
  });

  it("hides modal", () => {
    const action: ILayoutHideModal = {
      type: LAYOUT_HIDE_MODAL,
    };

    const expectedState = {...initialState, isModalOpen: false};

    expect(layoutReducer(initialState, action)).toEqual(expectedState);
  });
});
