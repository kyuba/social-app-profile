import commentsReducer, {
  COMMENTS_ADD,
  COMMENTS_SET,
  IComment,
  ICommentsAddACtion,
  ICommentsSetAction,
} from "../comments";

const initialState: IComment[] = new Array();

const mockComments = [
  {
    uid: "3a017fc5-4f50-4db9-b0ce-4547ba0a1bfd",
    username: "Mike Ross",
    date: "19.11.2018",
    message: "Lorem ipsum dolor sit amet enim. Etiam ullamcorper.",
  },
  {
    uid: "28461231-b3d4-4450-8c8c-ff749a0aa642",
    username: "Rachel Zane",
    date: "20.11.2018",
    message: "Lorem ipsum dolor sit amet enim. Etiam ullamcorper.",
  },
];

describe("commentsReducer", () => {
  it("sets comments", () => {
    const action: ICommentsSetAction = {
      type: COMMENTS_SET,
      payload: [...mockComments],
    };

    const expectedState = [...action.payload];

    expect(commentsReducer(initialState, action)).toEqual(expectedState);
  });

  it("adds new comment to already existing ones", () => {
    const action: ICommentsAddACtion = {
      type: COMMENTS_ADD,
      payload: {
        uid: "b2d390b8-5e7f-40d5-909a-8455bdee2b4b",
        username: "Louis Litt",
        date: "20.11.2018",
        message: "Lorem ipsum dolor sit amet enim. Etiam ullamcorper.",
      },
    };

    const expectedState = [...mockComments, action.payload];

    expect(commentsReducer(mockComments, action)).toEqual(expectedState);
  });
});
