// Typings
export interface ILayout {
  hasError: boolean;
  isModalOpen: boolean;
  errorMsg: string;
}

export interface ILayoutShowError { type: typeof LAYOUT_SHOW_ERROR; payload: string; }
export interface ILayoutHideError { type: typeof LAYOUT_HIDE_ERROR; }
export interface ILayoutShowModal { type: typeof LAYOUT_SHOW_MODAL; }
export interface ILayoutHideModal { type: typeof LAYOUT_HIDE_MODAL; }
type LayoutReducerActions = ILayoutShowError | ILayoutHideError | ILayoutShowModal | ILayoutHideModal;

// Actions
export const LAYOUT_SHOW_ERROR =  "social-app-profile/layout/SHOW_ERROR";
export const LAYOUT_HIDE_ERROR =  "social-app-profile/layout/HIDE_ERROR";
export const LAYOUT_SHOW_MODAL =  "social-app-profile/layout/SHOW_MODAL";
export const LAYOUT_HIDE_MODAL =  "social-app-profile/layout/HIDE_MODAL";

export const showError = (err: string) => ({type: LAYOUT_SHOW_ERROR, payload: err});
export const hideError = () => ({type: LAYOUT_HIDE_ERROR});
export const showModal = () => ({type: LAYOUT_SHOW_MODAL});
export const hideModal = () => ({type: LAYOUT_HIDE_MODAL});

// Reducer
const initialState = {
  hasError: false,
  isModalOpen: false,
  errorMsg: "",
};

export default function layoutReducer(state: ILayout = initialState, action: LayoutReducerActions) {
  switch (action.type) {
    case LAYOUT_SHOW_ERROR:
      return {
        ...state,
        hasError: true,
        errorMsg: action.payload,
      };
    case LAYOUT_HIDE_ERROR:
      return {
        ...state,
        hasError: false,
        errorMsg: "",
      };
    case LAYOUT_SHOW_MODAL:
      return {
        ...state,
        isModalOpen: true,
      };
    case LAYOUT_HIDE_MODAL:
      return {
        ...state,
        isModalOpen: false,
      };
    default:
      return state;
  }
}
