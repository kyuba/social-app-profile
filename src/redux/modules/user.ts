import { Action } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { LAYOUT_SHOW_ERROR } from "./layout";
import { IApplicationState } from "./rootReducer";

// Typings
interface ISocial {
  likes: number;
  following: number;
  followers: number;
}
export interface IUser {
  name: string;
  city: string;
  country: string;
  social: ISocial;
}

export interface IUserSetAction { type: typeof USER_SET; payload: IUser; }
export interface IUserIncStatAction { type: typeof USER_INC_SOCIAL_STAT; payload: keyof ISocial; }

type UserReducerActions = IUserSetAction | IUserIncStatAction;

// Actions
export const USER_SET = "social-app-profile/user/SET";
export const USER_INC_SOCIAL_STAT = "social-app-profile/user/INC_SOCIAL_STAT";

export const setUser = () => async (dispatch: ThunkDispatch<IApplicationState, void, Action>): Promise<Action> => {
  try {
    const res = await fetch("__mocks__/user.json");
    const json = await res.json();

    return dispatch({
      type: USER_SET,
      payload: json,
    });
  } catch (error) {
    return dispatch({
      type: LAYOUT_SHOW_ERROR,
      payload: error.message,
    });
  }
};

export const incSocialStat = (stat: string) => {
  return {
    type: USER_INC_SOCIAL_STAT,
    payload: stat,
  };
};

// Reducer
const initialState = {
  name: "",
  city: "",
  country: "",
  social: {
    likes: 0,
    following: 0,
    followers: 0,
  },
};

export default function userReducer(state: IUser = initialState, action: UserReducerActions) {
  switch (action.type) {
    case USER_SET:
      return {
        ...action.payload,
      };
    case USER_INC_SOCIAL_STAT:
      return {
        ...state,
        social: {
          ...state.social,
          [action.payload]: state.social[action.payload] + 1,
        },
      };
    default:
      return state;
  }
}
