import React, { Component } from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import { hideModal, ILayout } from "src/redux/modules/layout";
import { IApplicationState } from "src/redux/modules/rootReducer";

import "./Modal.scss";

interface IProps {
  hideModal: () => void;
  layout: ILayout;
}

class Modal extends Component<IProps> {
  private modalContentRef: React.RefObject<HTMLDivElement>;

  constructor(props: IProps) {
    super(props);

    this.modalContentRef = React.createRef();
  }

  close = () => {
    this.props.hideModal();
  }

  onOverlay = (event: React.MouseEvent<HTMLInputElement>) => {
    if (this.modalContentRef.current && !this.modalContentRef.current.contains(event.target as Node)) {
      this.props.hideModal();
    }
  }

  render() {
    return (
      <div id="modal" className={`modal ${this.props.layout.isModalOpen ? "--open" : ""}`} onClick={this.onOverlay}>
        <div className="modal__content" ref={this.modalContentRef}>
          <span className="close" onClick={this.close}>&times;</span>
          <input type="text" defaultValue={window.location.href} autoFocus/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IApplicationState) => ({
  layout: state.layout,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  hideModal: () => dispatch(hideModal()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
