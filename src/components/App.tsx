import React from "react";
import { Provider } from "react-redux";

import store from "src/redux/store";

import Modal from "./Modal/Modal";
import Profile from "./Profile/Profile";

import "./App.scss";

const App: React.SFC = () => {
  return (
    <Provider store={store}>
      <div className="app">
        <Profile />
        <Modal />
      </div>
    </Provider>
  );
};

export default App;
