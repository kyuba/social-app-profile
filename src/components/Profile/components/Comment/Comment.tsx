import React from "react";

import UserAvatar from "../UserAvatar/UserAvatar";

import { IComment } from "src/redux/modules/comments";

import "./Comment.scss";

interface IProps {
  data: IComment;
}

const Comment: React.SFC<IProps> = (props: IProps) => {
  const { username, date, message } = props.data;

  return (
    <div className="comment">
      <div className="comment__avatar">
        <UserAvatar small={true}/>
      </div>
      <div className="comment__content">
        <div className="comment__info-row">
          <span>{username}</span>
          <span>{date}</span>
        </div>
        <div className="comment__message">
          {message}
        </div>
      </div>
    </div>
  );
};

export default Comment;
