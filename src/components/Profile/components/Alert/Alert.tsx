import React, { Component } from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import { hideError, ILayout } from "src/redux/modules/layout";
import { IApplicationState } from "src/redux/modules/rootReducer";

import "./Alert.scss";

interface IProps {
  hideError: () => void;
  layout: ILayout;
}

class Alert extends Component<IProps> {
  close = () => {
    this.props.hideError();
  }

  render() {
    return (
      <div className={`alert ${this.props.layout.hasError ? "--open" : ""}`}>
        <div className="alert__content">
          <span className="close" onClick={this.close}>&times;</span>
          <span>Error: {this.props.layout.errorMsg} </span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IApplicationState) => ({
  layout: state.layout,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  hideError: () => dispatch(hideError()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Alert);
