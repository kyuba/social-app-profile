import React, { Fragment, PureComponent } from "react";
import { connect } from "react-redux";
import { Action } from "redux";
import { ThunkDispatch } from "redux-thunk";

import { showModal } from "src/redux/modules/layout";
import { IApplicationState } from "src/redux/modules/rootReducer";
import { incSocialStat, IUser, setUser } from "src/redux/modules/user";

import Spinner from "../Spinner/Spinner";
import UserInfo from "../UserInfo/UserInfo";
import UserSocialStats from "../UserSocialStats/UserSocialStats";

import "./ProfileHeader.scss";

interface IProps {
  user: IUser;
  incSocialStat: (stat: string) => void;
  setUser: () => void;
  showModal: () => void;
}

class ProfileHeader extends PureComponent<IProps> {
  handleIncSocialStat = (stat: string) => (): void => {
    this.props.incSocialStat(stat);
  }

  componentDidMount() {
    this.props.setUser();
  }

  render() {
    return (
      <section className="profile-header">
        <a href="#" className="profile-header__share-handle" onClick={this.props.showModal}>
          <span className="icon-share-square-solid"/>
        </a>
        {this.renderInfoSection()}
      </section>
    );
  }

  private renderInfoSection() {
    if (this.props.user.name) {
      const { name, city, country } = this.props.user;
      const { likes, followers, following } = this.props.user.social;
      const userData = { name, city, country };

      return (
        <Fragment>
          <UserInfo data={userData} addLike={this.handleIncSocialStat("likes")}/>
          <UserSocialStats stats={{likes, following, followers}} addFollow={this.handleIncSocialStat("followers")}/>
        </Fragment>
      );
    }

    return <Spinner/>;
  }
}

const mapStateToProps = (state: IApplicationState) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<IApplicationState, void, Action>) => ({
  setUser: () => dispatch(setUser()),
  incSocialStat: (stat: string) => dispatch(incSocialStat(stat)),
  showModal: () => dispatch(showModal()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileHeader);
