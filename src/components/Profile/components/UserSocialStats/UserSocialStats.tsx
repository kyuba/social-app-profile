import React from "react";
import "./UserSocialStats.scss";

interface IProps {
  stats: {
    likes: number;
    following: number;
    followers: number;
    [key: string]: number;
  };
  addFollow: () => void;
}

const UserSocialStats: React.SFC<IProps> = (props: IProps) => {
  const { stats, addFollow } = props;

  const list = Object.keys(stats).map((key) => {
    return (
      <li key={key}>
        <a href="#" id={`${key}-handle`}>
          <span>{stats[key]}</span>
          <span>{key}</span>
        </a>
      </li>
    );
  });

  return (
    <div className="user-social-stats">
      <ul className="user-social-stats__list">
        {list}
      </ul>
      <a href="#" id="btn-follow" className="btn-follow" onClick={addFollow}>FOLLOW</a>
    </div>
  );
};

export default UserSocialStats;
