import React, { Component } from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import { showError } from "src/redux/modules/layout";

import Spinner from "../Spinner/Spinner";

interface IProps {
  showError: (err: string) => void;
}

interface IState {
  hasChildError: boolean;
}

class ErrorBoundry extends Component<IProps, IState> {
  state: IState = {
    hasChildError: false,
  };

  componentDidCatch(err: Error) {
    this.setState({hasChildError: true});
    this.props.showError(err.message);
  }

  render() {
    if (this.state.hasChildError) {
      return <Spinner/>;
    }
    return this.props.children;
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  showError: (err: string) => dispatch(showError(err)),
});

export default connect(null, mapDispatchToProps)(ErrorBoundry);
