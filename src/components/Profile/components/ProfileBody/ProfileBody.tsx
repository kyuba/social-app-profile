import React from "react";

import CommentsList from "../CommentsList/CommentsList";

import "./ProfileBody.scss";

const ProfileBody: React.SFC = () => {
  return (
    <div className="profile-body">
      <CommentsList />
    </div>
  );
};

export default ProfileBody;
