import React from "react";

import UserAvatar from "../UserAvatar/UserAvatar";

import "./UserInfo.scss";

export interface IData {
  name: string;
  city: string;
  country: string;
}

interface IProps {
  data: IData;
  addLike: () => void;
}

const UserInfo: React.SFC<IProps> = (props: IProps) => {
  const { name, city, country } = props.data;
  const { addLike } = props;

  return (
    <div className="user-info">
      <UserAvatar/>
      <div className="user-info__data">
        <h3>
          {name}
          <a
            href="#"
            id="like-handle"
            className="like-handle"
            onClick={addLike}
          >
            <span className="icon-heart-regular"/>
          </a>
        </h3>
        <span>{city}, {country}</span>
      </div>
    </div>
  );
};

export default UserInfo;
