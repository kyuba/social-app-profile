import { compareAsc, distanceInWordsStrict, format } from "date-fns";
import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Action } from "redux";
import { ThunkDispatch } from "redux-thunk";
import uuid from "uuid";

import { addComment, IComment, setComments } from "src/redux/modules/comments";
import { IApplicationState } from "src/redux/modules/rootReducer";

import Comment from "../Comment/Comment";
import Spinner from "../Spinner/Spinner";

import "./CommentsList.scss";

interface IProps {
  comments: IComment[];
  setComments: () => void;
  addComment: (comment: IComment) => void;
}

interface IState {
  showComments: boolean;
  message: string;
}

class CommentsList extends PureComponent<IProps, IState> {
  state: IState = {
    showComments: true,
    message: "",
  };

  private commentsListBox: React.RefObject<HTMLUListElement>;

  constructor(props: IProps) {
    super(props);

    this.commentsListBox = React.createRef();
  }

  componentDidMount() {
    this.props.setComments();
  }

  toggleComments = () => {
    this.setState({showComments: !this.state.showComments});
  }

  onMessageChange = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({message: event.currentTarget.value});
  }

  onSubmitComment = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const { message } = this.state;

    if (!message) {
      return;
    }

    const comment = {
      uid: uuid(),
      username: "Daniel Hardman", // hardcoded for demo purposes
      date: format(new Date(), "YYYY-MM-DDTHH:mm:ss"),
      message: this.state.message,
    };

    this.props.addComment(comment);

    this.setState({showComments: true, message: ""});

    this.scrollToLastComment();
  }

  render() {
    const { showComments } = this.state;
    const { comments } = this.props;

    return (
      <div className={`comments-list ${showComments ? "--open" : ""}`}>
        <form id="submit-comment-form" onSubmit={this.onSubmitComment}>
          <div className="comments-list__toggle-box">
            <a
              className="comments-list__toggle-box__handle"
              href="#"
              onClick={this.toggleComments}
            >
              {showComments ? "Hide comments" : "Show comments"}
              {` (${comments.length})`}
            </a>
          </div>
          <ul className="comments-list__box" ref={this.commentsListBox}>
            {this.renderComments()}
          </ul>
          <div className="comments-list__add-new">
            <input
              type="text"
              name="new-comment"
              id="new-comment"
              value={this.state.message}
              placeholder="Add a comment"
              onChange={this.onMessageChange}
              required={true}
            />
          </div>
        </form>
      </div>
    );
  }

  private renderComments = () => {
    const formatted = this.formatComments(this.props.comments);

    if (formatted.length) {
      return formatted.map((item) => <li key={item.uid}><Comment data={item}/></li>);
    }

    return <div className="comments-list__box__fallback">This user has no comments yet.</div>;
  }

  private formatComments = (comments: IComment[]) => {
    if (!comments.length) {
      return [];
    }

    return comments
        .sort((a, b) => compareAsc(a.date, b.date))
        .map((comm) => ({
          ...comm,
          date: distanceInWordsStrict(new Date(), comm.date, {unit: "d"}).replace(/ /g, "").substr(0, 2),
        }));
  }

  private scrollToLastComment = () => {
    setTimeout(() => {
      const container = this.commentsListBox.current;
      if (container) {
        container.scrollTop = container.scrollHeight - container.clientHeight;
      }
    });
  }
}

const mapStateToProps = (state: IApplicationState) => ({
  comments: state.comments,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<IApplicationState, void, Action>) => ({
  setComments: () => dispatch(setComments()),
  addComment: (comment: IComment) => dispatch(addComment(comment)),
});

export { CommentsList };
export default connect(mapStateToProps, mapDispatchToProps)(CommentsList);
