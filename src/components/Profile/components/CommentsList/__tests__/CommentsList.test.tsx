import { shallow } from "enzyme";
import React from "react";
import { CommentsList } from "../CommentsList";

describe("<CommentsList/>", () => {
  const props = {
    comments: new Array(),
    setComments: jest.fn(),
    addComment: jest.fn(),
  };

  beforeEach(() => {
    props.setComments.mockClear();
    props.addComment.mockClear();
  });

  it("toggles comments visibility", () => {
    const wrapper = shallow(<CommentsList {...props}/>);
    wrapper.setState({showComments: false});
    wrapper.find(".comments-list__toggle-box__handle").simulate("click");

    expect(wrapper.state("showComments")).toBe(true);
  });

  it("calls addComment on comment submit", () => {
    const wrapper = shallow(<CommentsList {...props}/>);
    const mockEvent = { preventDefault: jest.fn() };

    wrapper.setState({message: "Dummy message"});
    wrapper.find("#submit-comment-form").simulate("submit", mockEvent);

    expect(props.addComment.mock.calls.length).toBe(1);
  });

  it("prevents calling addComment when trying to submit empty comment", () => {
    const wrapper = shallow(<CommentsList {...props}/>);
    const mockEvent = { preventDefault: jest.fn() };

    wrapper.setState({message: ""});
    wrapper.find("#submit-comment-form").simulate("submit", mockEvent);

    expect(props.addComment.mock.calls.length).toBe(0);
  });

  it("clears message on comment submit", () => {
    const wrapper = shallow(<CommentsList {...props}/>);
    const mockEvent = { preventDefault: jest.fn() };

    wrapper.setState({message: "Dummy message"});
    wrapper.find("#submit-comment-form").simulate("submit", mockEvent);

    expect(wrapper.state("message")).toBeFalsy();
  });

  it("shows comments on comment submit", () => {
    const wrapper = shallow(<CommentsList {...props}/>);
    const mockEvent = { preventDefault: jest.fn() };

    wrapper.setState({showComments: false});
    wrapper.setState({message: "Dummy message"});
    wrapper.find("#submit-comment-form").simulate("submit", mockEvent);

    expect(wrapper.state("showComments")).toBe(true);
  });
});
