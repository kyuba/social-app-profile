import React from "react";
import avatar from "src/assets/img/avatar.png";
import "./UserAvatar.scss";

interface IProps {
  small?: boolean;
}

const UserAvatar: React.SFC<IProps> = ({small = false}) => {
  return (
    <div className={`user-avatar ${small ? "--small" : ""}`}>
      <img src={avatar} alt="Avatar"/>
    </div>
  );
};

export default UserAvatar;
