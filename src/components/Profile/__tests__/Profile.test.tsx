import { shallow } from "enzyme";
import React from "react";
import Profile from "../Profile";

describe("<Profile/>", () => {
  it("renders <Profile/>", () => {
    const wrapper = shallow(<Profile/>);

    expect(wrapper.find(".profile")).toHaveLength(1);
  });
});
