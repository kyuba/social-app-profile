import React from "react";

import "./Profile.scss";

import Alert from "./components/Alert/Alert";
import ErrorBoundry from "./components/ErrorBoundry/ErrorBoundry";
import ProfileBody from "./components/ProfileBody/ProfileBody";
import ProfileHeader from "./components/ProfileHeader/ProfileHeader";

const Profile: React.SFC = () => {
  return (
    <div className="profile">
      <Alert />
      <ErrorBoundry>
        <ProfileHeader />
        <ProfileBody />
      </ErrorBoundry>
    </div>
  );
};

export default Profile;
